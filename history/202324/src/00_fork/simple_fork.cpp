#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main () {
    int contador = 0;

    printf ("Comenzando la ejecución\n");

    pid_t id_hijo;
    pid_t id_padre;

    id_padre = getpid ();
    printf("Antes de bifurcar\n");

    contador ++;
    id_hijo = fork ();

    contador ++;
    printf ("Después de bifurcar\n");

    if ( id_hijo == 0 )
        printf ("Id. hijo: %li. padre: %li. Contador: %i \n", (long) getpid (), (long) id_padre, contador);
    else
        printf ("Id. padre: %li. hijo: %li. Contador: %i \n", (long) getpid (), (long) id_padre, contador);


    return EXIT_SUCCESS;
}
