# Programación en Red

## Modelos de Red

| Capa | OSI             | TCP/IP          |
|:----:|:---------------:|:---------------:|
| 1    | APLICACIÓN      | APLICACIÓN      |
| 2    | PRESENTACIÓN    | ^ |
| 3    | SESIÓN          | ^ |
| 4    | TRANSPORTE      | TRANSPORTE      |
| 5    | RED             | INTERNET        |
| 6    | ENLACE DE DATOS | INTERFAZ DE RED |
| 7    | FÍSICO          | ^ |


## Protocolos

| Puerto | Siglas | Nombre                            |
|:------:|:-------|:----------------------------------|
| 21     | FTP    | File Transfer Protocol            |
| 22     | SSH    | Secure Shell                      |
| 23     | Telnet |                                   |
| 25     | SMTP   | Simple Mail Transfer Protocol     |
| 80     | HTTP   | Hyper Text Transfer Protocol      |
| 110    | POP3   | Post Office Protocol 3            |
| 143    | IMAP   | Internet Message Access Protocol  |
| 443    | HTTPS  | Hyper Text Transfer Procol Secure |


