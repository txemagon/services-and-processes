#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

enum TLang {es,en,pt,it,de};

const char *greeting[] = {
    "Hola",
    "Hello",
    "olá",
    "ciao",
    "hallo"
};

struct TParameter {
    int count;
    enum TLang lang;
};

void *
print (void *parameters)
{
    struct TParameter p = * ((struct TParameter *) parameters) ;

    for (int i=0; i<p.count; i++)
        fprintf (stderr, "%s ", greeting[p.lang]);

    return NULL;
}

int
main (int argc, char *argv[])
{
    pthread_t thread_id[2];
    struct TParameter call1 = { 7000, es };

    struct TParameter call2;
    call2.count = 5000;
    call2.lang = en;

    pthread_create (&thread_id[0], NULL, &print, (void *) &call1);
    pthread_create (&thread_id[1], NULL, &print, (void *) &call2);

    for (int i=0; i<2; i++)
        pthread_join (thread_id[i], NULL);

    fprintf (stderr, "\nThread terminated.\n\n");

    return EXIT_SUCCESS;
}
