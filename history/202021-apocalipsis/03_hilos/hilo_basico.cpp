#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

#define MAX 0x10

struct params {
    char caracter;
    int contador;
};



void* char_imprimir (void* parametros){
    struct params param = *(struct params *) parametros;

    for (int i = 0; i < param.contador; ++i)
        fputc (param.caracter, stderr);

    return NULL;
}



int main (int argc, char *argv[]){
    pthread_t thread_id[MAX];

    struct params thread_args;


#define LETRAS ('z' - 'a')

    for (int i=0; i<MAX; i++) {
      thread_args.caracter = rand() % LETRAS + 'a';
      thread_args.contador = rand() % 20000 + 10000;

      pthread_create (&thread_id[i], NULL, &char_imprimir, &thread_args);
    }

    // Do some lengthy stuff


    for (int i=0; i<MAX; i++)
      pthread_join (thread_id[i], NULL);

    fputc ('\n', stderr);

    return EXIT_SUCCESS;
}
