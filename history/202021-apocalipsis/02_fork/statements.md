1. El saber es una mata de cultivo propio.
1. El responsable de la educación es la familia.
1. La consciencia crece bajo efecto de la atención.
1. Una clase reparte la cantidad de atención recibida por el alumno
y la minora sobre la que podría recibir en familia.
1. Establecer un objetivo (o un indicador para medirlo) lleva
a trampear y cumplir con la letra en vez de con el espirítu de la letra.
1. La formación ténica es buena para la sociedad.
1. La formación humana es buena para la persona.
1. La sociedad es enemiga de la persona y vive de ella.
1. Quitar los exámenes es eximir de los resultados y por tanto eximir
de los logros y del crecimiento personal.
1. Cuando se establecen objetivos sociales y el individuo no los
establece como objetivos personales la única manera de cumplir
con las estadísticas es regalar la consecución de los objetivos.
1. El sistema educativo se le impone al individuo desde la infancia
y, por tanto, está lleno de individuos desmotivados.
1. El sistema educativo califica a la persona y por su naturaleza
competitiva trauma a los individuos de por vida.
1. Los sistemas una vez establecidos tienen instinto de supervivencia
y justifican su necesarieidad para no extinguirse.
1. Para el desarrollo del individuo es necesario tener medios para
experimentar y que éste pueda desarrollarse.
1. En el proceso didáctico los medios no son necesarios. La transmisión
de conceptos y la estructuración de la mente tiene una depencia baja de los medios. Sin embargo, la asimilación depende de la experimentación con los recursos. El recurso más importante para la experimentación es el tiempo.
1. La motivación por el estudio es ajena y debe ser previa al proceso formativo.
1. La formación nunca puede ser sistematizada. Es una relación y es una relación personal. La trayectoria y el objetivo de la misma no le pertenece al hombre.

