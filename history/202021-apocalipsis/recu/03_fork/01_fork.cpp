#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

void
lanzar (char *lc[])
{
    pid_t tengo_hijo = fork ();

    if (tengo_hijo == 0) {
        execvp ( lc[0], lc );
        fprintf (stderr, "Si hemos llegado hasta aquí: malo.\n");
        exit (1);
    }
}

int
main (int argc, char *argv[])
{
    lanzar (&argv[1]);

    return EXIT_SUCCESS;
}
