#ifndef __ARITMETICA_H__
#define __ARITMETICA_H__

#ifdef __cplusplus
extern "C"
{
#endif
  double add(double, double);
  double sub(double, double);
  double mul(double, double);
  double div(double, double);

#ifdef __cplusplus
}
#endif

#endif
