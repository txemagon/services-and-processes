function A () {}
A.ruedas = 5    // Sólo de la clase. No lo conocen los objetos de A
                // Sí lo conocen los objetos de B


B.prototype = new A
function B()
{
  this.puertas = 4
}

B.prototype.n_likes = 5 // Compartido por todos los objetos

var b = new B()
var c = new B()
c.puertas += 1

b.puertas
=> 4
c.puertas
=> 5

b.n_likes += 1
b.n_likes
=> 6
c.n_likes
=> 6
