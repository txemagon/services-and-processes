function Contenedor(html) {
  if (html) {
    this.html_block = document.getElementById(html)
      if (!this.html_block) {
          var container = document.getElementsByClassName(html)[0]
          if (!container)
              throw "Invalid container: " + html
          this.html_block = document.createElement("div")
          this.html_block.classList.add("Block")
          container.appendChild(this.html_block)
      }
    this.output = document.createElement("p")
    this.html_block.appendChild(this.output)
  }
}

Contenedor.prototype.set = function () {
  this.output.innerHTML += "#"
}

Contenedor.prototype.init = function () {
  this.step()
}

Contenedor.prototype.step = function (interval) {
  /* Call me again */
  var that = this
  setTimeout(function() {
    that.step()
  }, interval)
}
