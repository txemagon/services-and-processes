function main() {
  var semaphore = new Semaphore()

  /* Agent */
  var fabrica = new Fabrica(semaphore, "fabrica")

  /* Bare Container */
  var muro = new Muro("muro")

  /* Agent */
  for (let i=0; i<2; i++)
    new Worker(fabrica, muro, semaphore, "Obrero")

  /* Start agents */
  Agent.init()
}
