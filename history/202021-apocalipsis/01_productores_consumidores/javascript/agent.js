Agent.prototype = new Contenedor
Agent.prototype.constructor = Agent
Agent.superclass = Contenedor

function Agent(semaphore) {
    var args = arguments
    if (typeof(semaphore) !== 'undefined' && semaphore instanceof Semaphore) {
        this.semaphore = semaphore
        args = Array.prototype.slice.call(arguments, 1)
        Contenedor.apply(this, args)
        Agent.add(this)
    }
}

Agent.pool = []

Agent.add = function(agent) {
    Agent.pool.push(agent)
}

Agent.init = function () {
    for (agent of Agent.pool)
        agent.init()
}
