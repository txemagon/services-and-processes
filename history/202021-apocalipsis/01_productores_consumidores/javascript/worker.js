Worker.prototype = new Agent
Worker.prototype.constructor = Worker
Worker.superclass = Agent

function Worker(src, dst, semaphore, html_class) {
    if (arguments.length) {
        this.src = src
        this.dst = dst
        var args = Array.prototype.slice.call(arguments, 2)
        Agent.apply(this, args)
    }
}

Worker.id = 0
Worker.max_time = 2

Worker.get_id = function () {
    Worker.id += 1
    return Worker.id
}


Worker.prototype.take = async function () {
    this.src.gimme()
    this.output.innerHTML = "#"
    var nap_time = Math.random() * 10
    await sleep(nap_time)
    this.output.innerHTML = ""
    this.dst.set()
}

Worker.prototype.step = async function() {
    /* Do own stuff */
    await ((this.semaphore.wait())())
    await this.take()

    /* Calculate time to next stuff */
    var interval = Math.random() * Worker.max_time * 1000

    Contenedor.prototype.step.call(this, interval)
}
