/* Herencia Prototípica */
Fabrica.prototype = new Agent
Fabrica.prototype.constructor = Fabrica
Fabrica.superclass = Agent

function Fabrica(semaphore, html_id, max_storage) {
  Agent.apply(this, arguments)
  this.max_storage = max_storage || 15
  this.storage = 0
  this.slowness = 10
}

Fabrica.prototype.produce = function() {
  this.output.innerHTML += "#"
  this.storage += 1
  this.semaphore.signal()
}

Fabrica.prototype.gimme = function() {
  this.output.innerHTML = ""
  this.storage -= 1
  for (let i=0; i<this.storage; i++)
    this.output.innerHTML += "#"
}

Fabrica.prototype.step = function() {
  /* Do own stuff */
  this.produce()

  if (this.storage >= this.max_storage)
    return

  /* Calculate time to next stuff */
  var interval = Math.random() * this.slowness * 1000

  Contenedor.prototype.step.call(this, interval)
}
