Semaphore.idn = 0

function Semaphore(count) {
    this.idn = Semaphore.idn += 1
    this.count = count || 0
    this.wait_id = 0
    this.pool = []

    Semaphore.debug = Semaphore.debug || document.getElementById("semaphore_debug")
}

Semaphore.prototype.debug = function(mssg) {
    if (!Semaphore.debug)
        return

    var out = Semaphore.debug
    var time = new Date()
    out.innerHTML  = "[" + time.getHours() + ":" + time.getMinutes() +":" + time.getSeconds() + "]<br/>\n"
    out.innerHTML += "message:  " + mssg + "<br/>\n"
    out.innerHTML += "<br/>\n"
    out.innerHTML += "Semaphore " + this.idn + "<br/>\n"
    out.innerHTML += "  count: " + this.count  + "<br/>\n"
    out.innerHTML += "  waits: " + this.pool.toString()  + "<br/>\n"
}

/* Catalog of client executable wait functions */
Semaphore.semaphore_free = function(that) {
    return async function () {
        await that.createPromise(false)
    }
}


Semaphore.semaphore_occupied = function(that) {
    return async function () {
        await that.createPromise(true)
    }
}


Semaphore.prototype.end_wait = function() {
    this.pool.shift()
}

Semaphore.prototype.signal = function () {
    if (this.pool.length)
        this.end_wait()
    else
        this.count += 1

    this.debug("Signal called")
}


Semaphore.prototype.get_wait_id = function () {
    this.wait_id += 1
    return this.wait_id
}

Semaphore.prototype.wait_unlock = async function(wait_id) {
    while (this.pool.includes(wait_id)) {
      await sleep(0.5)
    }
}

Semaphore.prototype.createPromise = function(busy) {
    if (!busy) {
        var promise = Promise.resolve("Semaphore " + this.idn + ": Free on arrival.")
        return promise
    }


    var wait_id = this.get_wait_id()
    this.pool.push(wait_id)
    this.debug("Wait called")
    var that = this
    return new Promise( async function(resolve) {
        await that.wait_unlock(wait_id)
        resolve('Semaphore " + this.idn + ": Released for wait ' + wait_id)
    } )
}

Semaphore.prototype.wait = function () {

    var client_wait

    if (this.count) {
        this.count -= 1
        client_wait = Semaphore.semaphore_free(this)
    } else
        client_wait = Semaphore.semaphore_occupied(this)

    return client_wait
}
