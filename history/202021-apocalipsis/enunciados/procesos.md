# Enunciados de Procesos

## Teoría

### Pregunta 1

Predice la salida del siguiente programa.

```c

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
int main()
{

    // make two process which run same
    // program after this instruction
    fork();

    printf("Hello world!\n");
    return 0;
}
```

### Pregunta 2

Calcula la cantidad de veces que se imprime `"Hola"`

```c
#include <stdio.h>
#include <sys/types.h>
int main()
{
	fork();
	fork();
	fork();
	printf("hello\n");
	return 0;
}
```

### Pregunta 3

Predice la salida del siguiente programa.

```c

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
void forkexample()
{
    // child process because return value zero
    if (fork() == 0)
        printf("Hello from Child!\n");

    // parent process because return value non-zero.
    else
        printf("Hello from Parent!\n");
}
int main()
{
    forkexample();
    return 0;
}
```

### Pregunta 4

Predice la salida del siguiente programa:


```c
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void forkexample()
{
	int x = 1;

	if (fork() == 0)
		printf("Child has x = %d\n", ++x);
	else
		printf("Parent has x = %d\n", --x);
}
int main()
{
	forkexample();
	return 0;
}

```

### Pregunta 5

¿Cuántos procesos creará el siguiente código?


```c
for (i = 0; i < n; i++)
	fork();
```

### Pregunta 6

Predice el resultado de la ejecución del siguiente código.

```c
#include <stdio.h>
#include <unistd.h>
int main()
{
	fork();
	fork() && fork() || fork();
	fork();

	printf("forked\n");
	return 0;
}

```

## Práctica

### Problema 1

Crear un árbol de procesos (`fork`) recurrente que imprima la sucesión de fibonacci. Resolverlo usando
`exec`.

### Problema 2
Crear un árbol de procesos (`fork`) recurrente que imprima la sucesión de fibonacci. Resolverlo sin usar
`exec`.

### Problema 3

Confeccionar un programa que:

1. Impida el `Ctl C` poniendo `No está permitido el uso de Ctl C`.
1. Atienda a la señal `SIGUSR1` y cuente la cantidad de veces que ha sido enviada mediante
el comando `kill`
1. Cree un proceso hijo que vaya empujando en una pila dinámica (`realloc`) los números
primos.
1. Su proceso hijo termine cuando reciba la señal `SIGINT`. Al recibirla imprimirá todos los números primos en el fichero temporal.
1. Cuando muera su proceso hijo salga del bucle de espera, imprima todos los números primos que hay en el
fichero temporal y la cantidad de veces que ha sido enviada la señal `SIGUSR1`.
1. Limpie asíncronamente el valor de retorno del proceso hijo.

## Bibliografía

https://www.geeksforgeeks.org/fork-system-call/

