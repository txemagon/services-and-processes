#include "cli_parsing.h"

#include <stdlib.h>
#include <string.h>
#include <getopt.h>

#define MAX 0x800




/******************/
/* Default values */
/******************/

const int default_argc = 1;
char *default_argv[] = {
    "fork",
    NULL
};





/****************/
/* Private vars */
/****************/

int init = 0;
char command_name[MAX];


#define USAGE_STR  "                                         \n\
    Usage:       %1$s [options] [<fibo1> [<fibo2>]]          \n\
                                                             \n\
    Figures out the next term of a fibonacci serie.          \n\
                                                             \n\
                                                             \n\
    Example:                                                 \n\
                %1$s 3 5                                     \n\
                => 8                                         \n\
                                                             \n\
                                                             \n\
    Parameters:                                              \n\
                                                             \n\
      fibo1:        Previous number in the fibonacci serie.  \n\
                    Defaults to 1.                           \n\
      fibo2:        Previous number in the fibonacci serie.  \n\
                    Defaults to 0.                           \n\
                                                             \n\
    Options:                                                 \n\
                                                             \n\
      -c, --countdown:                                       \n\
                    Number of remaining terms in the serie.  \n\
                    Defaults to 0.                           \n\
                                                             \n\
                                                             \n\
      -h, --help:                                            \n\
                    Show this help.                          \n\
                                                             \n\
      -v, --verbose:                                         \n\
                    Shows command line parsing data.         \n\
                                                             \n\
"




/***********************************/
/* Absolutely necessary prototypes */
/***********************************/

struct TCLIOption
parse (int argc, char *argv[]);





/*********************/
/* Private functions */
/*********************/


/**
 * Set init flag to 1
 */
void
init_unit (int argc, char *argv[])
{
    init = 1;       /* Compilation Unit has been inited */


    if (init)       /* Compilation Unit already initialized */
        return;

    if (argv)       /* Initialized through parse function */
        return;

    /* Default Initalization */
    parse(default_argc, default_argv);
}



/**
 * Print parsed params
 */
void
print_run_params (struct TCLIOption pd)
{
    printf (
            "Previous terms: %i, %i\n",
            pd.previous[0], pd.previous[1]
            );
    printf ("Remaining terms: %i\n", pd.countdown);
}

/********************/
/* Public functions */
/********************/


/**
 * Parse Command line arguments
 */
struct TCLIOption
parse (int argc, char *argv[])
{
    struct TCLIOption parsed_data = { {1, 0}, 0 };

    /* Mark compilation unit as inited */
    init_unit(argc, argv);

    /* Guess command name*/
    strncpy (command_name, argv[0], MAX-1);      /* Save argv[0] just in case someone else changes it */
    command_name[MAX-1] = '\0';                  /* If the name is too large ensures is zero finished */

    /* Parse options */
    const char * const short_options = "c:hv";    /* Remember ':' means => option needs an argument */
    const struct option long_options[] = {
        { "countdown", 1, NULL, 'c' },
        { "help",      0, NULL, 'h' },
        { "verbose",   0, NULL, 'v' },
        { NULL,        0, NULL, 0 }              /* Sentinel value */
    };

    /* Status variable */
    int verbose = 0;

    int next_option;

    do {
        next_option = getopt_long (
                argc,
                argv,
                short_options,
                long_options,
                NULL
                );

        switch (next_option) {
            case 'h':
                usage(stdout);
                exit (EXIT_SUCCESS);
                break;

            case 'v':
                parsed_data.verbose = verbose = 1;
                break;


            case 'c':
                parsed_data.countdown = atoi (optarg);
                break;

            case '?':     /* Invalid Option */
                usage(stderr);
                exit (EXIT_FAILURE);
                break;

            case -1:      /* No more options available */
                break;

            default:     /* Something unexpected occurred */
                abort ();

        }

    } while ( next_option != -1 );

    /* Parse arguments */
    for (int i = optind; i < argc; i++)
        parsed_data.previous[i - optind] = atoi (argv[i]);

    if (verbose)
        print_run_params (parsed_data);

    return parsed_data;
}



/**
 * Print usage
 */
void
usage(FILE *outstream)
{
    init_unit(0, NULL);      /* Ensure we've already got a command name */
    fprintf(outstream, USAGE_STR, command_name);
}
