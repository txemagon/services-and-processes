/*
 * =====================================================================================
 *
 *       Filename:  main.cpp 1.0 28/10/20 19:39:19
 *
 *    Description: Solving fibonnacci series using fork using fork and exec
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include "cli_parsing.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>


pid_t
spawn (struct TCLIOption opt)
{ // ./fork 5 3 4 => ./fork 8 5 3
    pid_t i_have_a_child = fork ();

    if (!i_have_a_child) {          // To debug a child process in gdb:
        if (opt.countdown > 0) {    // set follow-fork-mode child

            /* Roll parameters */
            int sum = opt.previous[1] + opt.previous[0];
            opt.previous[1] = opt.previous[0] ;
            opt.previous[0] = sum;
            opt.countdown--;

            /* Set the command line up */
            char param[3][0x10];
            sprintf (param[0], "%i", opt.previous[0] );
            sprintf (param[1], "%i", opt.previous[1] );
            sprintf (param[2], "%i", opt.countdown );

            execl (command_name, command_name, "-c", param[2], param[0], param[1], (char *) NULL);
            fprintf (stderr, "Fork failed!.\n");
            exit (EXIT_FAILURE);
        } else
            exit (0);  // Countdown == 0 => End reached.
    }

    return i_have_a_child;
}

int
main (int argc, char *argv[])
{
    /* Variables */
    pid_t child_pid = 0;
    int child_status;
    struct TCLIOption option;

    option = parse (argc, argv);

    /* Verbose launching information */
    if (option.verbose) {
        for (int i=0; i<argc; i++)
            fprintf (stderr, "%s ", argv[i]);
        fprintf (stderr, "\n");
    }

    /* Subprocess launching */
    child_pid = spawn (option);
    wait (&child_status);

    if (child_pid)
        printf( "%i ", option.previous[0] + option.previous[1] );

    if ( !WIFEXITED (child_status) ) {
        fprintf (stderr, "\nBroken chain!\n\n");
        return EXIT_FAILURE;
    }

    printf ("\n");

    return EXIT_SUCCESS;
}
