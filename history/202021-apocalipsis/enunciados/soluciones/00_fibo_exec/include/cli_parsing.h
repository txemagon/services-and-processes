/*
 * =====================================================================================
 *
 *       Filename:  cli_parsing.h 1.0 28/10/20 19:42:08
 *
 *    Description:  Parse Options from command line for fibonacci fork
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 *
 * =====================================================================================
 */

#ifndef __CLI_PARSING_H__
#define __CLI_PARSING_H__

#include <stdio.h>

struct TCLIOption
{
    int previous[2];   // Fibonacci previous terms
    int countdown;     // Remaining steps to end printing the sequence
    int verbose;       // Show extra information
};

extern char command_name[];

/* Public Interface */
#ifdef __cplusplus
extern "C"
{
#endif

    struct TCLIOption parse(int argc, char *argv[]);
    void              usage(FILE *outstream);

#ifdef __cplusplus
}
#endif
#endif
