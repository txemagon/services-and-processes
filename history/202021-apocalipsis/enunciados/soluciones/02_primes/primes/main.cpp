/*
 * =====================================================================================
 *
 *       Filename:  main.cpp 1.0 07/11/20 13:31:41
 *
 *    Description:  Calculate n primes
 *                  Do not run this program directly. Is meant to be launch in a fork.
 *
 *                  primes <nprimes> <output_file>
 *
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>

int
can_be_prime (unsigned p)
{
    static unsigned last_p = 1;

    if (p == 2) {
        last_p = 2;
        return 1;
    }

    static uint64_t pot = 1;
    /* Fermat's little theorem */
    for (unsigned i=0; i<p-1 - last_p; i++)
        r = (r << 1) % p;

    return r == 1;
}

int
is_prime (unsigned p, unsigned *prime, unsigned found)
{
    int is = 1;

    for (int i=0; is && i<found; i++)
        if (p % prime[i] == 0)
            is = 0;

    return is;
}

int
main (int argc, char *argv[])
{
    unsigned *prime = NULL;
    unsigned found   = 0;
    unsigned n_primes;
    pid_t parent = getppid ();

    /* CLI parsing */
    if (argc < 3)
        exit (EXIT_FAILURE);

    n_primes = atoi (argv[1]);
    prime = (unsigned *) malloc (n_primes * sizeof (unsigned));
    char *filename = argv[2];


    /* Do calculate */
    for (unsigned i=2; found<n_primes; i++)
        if (can_be_prime (i) && is_prime (i, prime, found)) {
            prime[found++] = i;
            kill (parent, SIGUSR1);
#ifdef NCOALESCE
            usleep (100000);
#endif
        }


    /* Do print results */
    int pfd = open (filename, O_WRONLY);
    unlink (filename);
    write (pfd, prime, found * sizeof (unsigned));
    close (pfd);

    free (prime);

    return EXIT_SUCCESS;
}
