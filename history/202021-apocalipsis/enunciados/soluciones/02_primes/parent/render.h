#ifndef __RENDER_H__
#define __RENDER_H__



#ifdef __cplusplus
extern "C"
{
#endif

void render (unsigned primes, unsigned total_primes);

#ifdef __cplusplus
}
#endif

#endif
