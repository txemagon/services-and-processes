#include "render.h"
#include "ansi.h"
#include "amstrad.h"

#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <time.h>
#include <math.h>


const uint64_t number[] = { A0, A1, A2, A3, A4, A5, A6, A7, A8, A9 };
const char *uni_block[] = { "█", "▓", "▒", "░" };
const int uni_len = 4;

const int xc = 40;
const int yc = 25;

#define SEG    280
#define ASTEP   2.
#define OSTEP  10.



#define TITLE "\
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█▀█░█▀▄░▀█▀░█▄█░█▀▀░█▀▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░\n\
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░█▀▀░█▀▄░░█░░█░█░█▀▀░▀▀█░░░░░░░░░░░░░░░░░░░░░░░░░░░░\n\
░░░░░░░░░░░░░░░░░░░░░░░░░░░░░▀░░░▀░▀░▀▀▀░▀░▀░▀▀▀░▀▀▀░░░░░░░░░░░░░░░░░░░░░░░░░░░░\n\
"

void
circle (double r, int xc, int yc)
{
    static double angle = .0;

    double astep = ASTEP * M_PI / 180.;

    ANS(COLOR, GREENL);

    for (double a=angle; a<angle+SEG; a+= ASTEP) {
        double arad = M_PI * a / 180;
        ANS(
                LOCA_CURS,
                (int) round ((r * cos (arad) / 2 + yc) ),
                (int) round ((r * sin (arad) + xc)     )
           );
        printf ("%s", uni_block[3 - ((int) (a - angle) / (SEG / 4) ) % 4]);
    }

    ANS(COLOR, RESET);

    angle += OSTEP;
    while (angle > 360)
        angle -= 360;
}






void
amstrad_print (uint64_t symbol, int x, int y)
{

    ANS(COLOR, GREENL);
    ANS(LOCA_CURS, y, x);


    for (unsigned row=0;  symbol>0; row++, symbol>>=8) {
        unsigned char line = symbol % 0x100;
        for (unsigned char mask=0x80; mask>0; mask>>=1)
            if (line & mask)
                printf ("%s", line & mask ? uni_block[0] : " " );
            else
                ANS(RGHT_CURS, 1);
        ANS(LOCA_CURS, y - row, x);
    }

    ANS(COLOR, RESET);
}





void
print_digit(unsigned digit, int x, int y)
{
    uint64_t graph = number[digit % 10];
    amstrad_print (graph, x, y);
}





void
print_number(unsigned n, int x, int y, int total)
{

    unsigned digits = (unsigned) ceil ( log10 (total + 1) );

    if (digits > 3){
        digits = 1;
        circle (12., xc, yc );
        amstrad_print (AM, x - 3, y + 3);
        ANS(LOCA_CURS, y + 15, x + 8);
        printf ("Primes: %i / %i\n", n, total);

    }  else {
        circle ( 4 + 2 * 4. * digits, xc, yc);

        if (n > 0)
            for (int digit=0; n>0; digit++, n/=10)
                print_digit (n % 10, x - 3 - 8 * digit + digits * 2, y + 3);
        else
            print_digit (n % 10, x - 3 , y + 3);
    }
}


void
title ()
{
    ANS(LOCA_CURS, 0, 0);
    printf("%s", TITLE);
}


void
render (unsigned primes, unsigned total_primes)
{

    int rv;
    struct timespec timt, timr;                                      // Total sleep time and remaining sleep time.
    timt.tv_sec  = 0;
    timt.tv_nsec = 300000000L;

    ANS(CLEAR_SCR);
    title ();
    //system ("toilet -fpagga '              PRIMES              '"); // Avoid more SIGCHLD's
    print_number (primes, xc, yc, total_primes);
    do {
        rv = nanosleep (&timt, &timr);                                // usleep and nanosleep are awaken on signal arrival
        timt = timr;                                                  // Set again timer with the remaining time
    } while (rv == -1);
}

