#ifndef __CLI_H__
#define __CLI_H__

#include "general.h"

#include <stdio.h>

struct TCLIOption
{
    int num_primes;
    int help;
    int verbose;
    char output_filename[MAXSTR];
};

extern char command_line[MAXSTR];
extern const struct TCLIOption * const option;

#ifdef __cplusplus
extern "C"
{
#endif

    void usage (FILE *out);
    void init_cli (int argc, char *argv[]);

#ifdef __cplusplus
}
#endif

#endif
