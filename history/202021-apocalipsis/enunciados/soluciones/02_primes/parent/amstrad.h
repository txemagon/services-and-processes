#ifndef __AMSTRAD_H__
#define __AMSTRAD_H__

#define A0       0x7CC6CED6E6C67C00
#define A1       0x1838181818187E00
#define A2       0x3C66063C60667E00
#define A3       0x3C46061C06663C00
#define A4       0x18385898FE183C00
#define A5       0x7E62603C06663C00
#define A6       0x3C66607C66663C00
#define A7       0x7E46060C18181800
#define A8       0x3C66663C66663C00
#define A9       0x3C66663E06663C00

#define AM       0x3838927C10282828

#endif
