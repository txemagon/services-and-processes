/*
 * =====================================================================================
 *
 *       Filename:  main.cpp 1.0 07/11/20 08:54:25
 *
 *    Description:  Launch prime calculator and wait.
 *
 *    Example:      ./controller 100000 -vo primes.ans
 *
 *        txemagon / imasen (), txema.gonz@gmail.com txemagon
 *
 *    GNU General Public License <https://www.gnu.org/licenses/>.
 * =====================================================================================
 */

/* Imports commad_line and options variables */
#include "cli.h"
#include "ansi.h"
#include "render.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <errno.h>



/********************/
/* GLOBAL VARIABLES */
/********************/
sig_atomic_t end       = 0;
sig_atomic_t new_prime = 0;
sig_atomic_t count = 0;

pid_t child;
int pfd;

/*********************/
/*  SIGNAL HANDLERS  */
/*********************/

void
end_time (int signum)
{
    if (signum == SIGTERM || signum == SIGINT)
        end = 2;
}

void
child_end (int signum)
{
    if (signum == SIGCHLD) {
        int status;
        pid_t end_child;
        wait (&status);
        end = 1;
    }

}

void
more_primes (int signum)
{
    new_prime++;
}





/*****************************/
/*  INITIALIZATION ROUTINES  */
/*****************************/

pid_t
spawn ()
{
    char primes[0x20];    // argv[1]
    char tmpfile[] = "/tmp/prime_file.XXXXXX";  // argv[2]

    sprintf (primes, "%i", option->num_primes);
    pfd = mkstemp (tmpfile);

    if (pfd == -1) {
        perror ("Couldn't open temporary file");
        exit (2);
    }

    pid_t child = fork ();

    if (!child) {
        execl ("./primes", "primes", primes, tmpfile, NULL);
        fprintf (stderr, "Couldn't launch primes program.\n");
        exit (EXIT_FAILURE);
    }

    return child;
}


void init_sft ()
{
    struct sigaction se;                // Control end signal
    bzero (&se, sizeof (se));
    se.sa_handler = &end_time;

    sigaction (SIGINT, &se, NULL);
    sigaction (SIGTERM, &se, NULL);


    struct sigaction sc;                 // Control child end signal
    bzero (&se, sizeof (sc));
    sc.sa_handler = &child_end;

    sigaction (SIGCHLD, &sc, NULL);      // If you use system every system call
                                         // will cast a SIGCHLD. You'll have to care
                                         // of the details.

    struct sigaction sp;                 // Control new prime signal
    bzero (&sp, sizeof (sp));            // If multiple signals of the
    sp.sa_handler = &more_primes;        // same type arrive before they're
                                         // attended, they will be coalesced.

    sigaction (SIGUSR1, &sp, NULL);

    child = spawn ();
}



void
print_results (unsigned primes)
{

    FILE *out = stdout;
    int file_out = 0;

    if (strcmp (option->output_filename, "") != 0)
        file_out = 1;

    if (file_out) {
        FILE *tmp = fopen(option->output_filename, "w");
        if (!tmp) {
            fprintf (stderr, "Couldn't open output file.\n");
            exit (3);
        }
        out = tmp;
    }

    struct stat stat;
    fstat (pfd, &stat);
    unsigned size = (unsigned) stat.st_size;
    unsigned *prime = (unsigned *) malloc (size);

    size_t total_read = read (pfd, prime, size);
    unsigned n_primes_read = size / sizeof (unsigned);
    for (unsigned *p = prime, i=0; p<prime+n_primes_read; i++, p++) {
        if (i % 20 == 0)
            fprintf (out, "\n" COLOR "%6i:\t" COLOR, GREENL, i, RESET);
        fprintf (out, "%i \t", *p);
    }
    fprintf (out, "\n");

    if (file_out)
        fclose (out);

    close (pfd);
    free (prime);

    if (option->verbose) {
        ANS(COLOR, GREEN);
        printf ("\n");
        printf ("Primes signaled: %i\n", primes);
        printf ("Signals coalesced: %i\n", option->num_primes - primes);
        printf ("Temporary file size: %i\n", size);
        printf ("Bytes read from file: %lu\n", total_read);
        printf ("Primes in file: %lu\n", size / sizeof (unsigned));
        printf ("File descriptor: %i\n", pfd);
        printf ("\n");
        ANS(COLOR, RESET);
    }
}

/******************/
/*  MAIN PROGRAM  */
/******************/

int
main (int argc , char *argv[])
{
    unsigned primes = 0;


    /* Initialization */
    init_cli (argc, argv);
    init_sft ();

    if (option->verbose)
        printf ("Primes: %i\n", option->num_primes);

    ANS(HIDE_CURS);
    ANS(SAVE_CURS);


    /* Main loop */
    while (!end) {
        primes += new_prime;
        new_prime = 0;
        render (primes, option->num_primes);
    }

    render (option->num_primes, option->num_primes);


    /* Finalization */
    ANS(REST_CURS);
    ANS(SHOW_CURS);

    /* Print results */
    print_results (primes);

    return end - 1;
}
