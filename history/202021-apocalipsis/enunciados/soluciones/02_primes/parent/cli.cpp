#include "cli.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>


struct TCLIOption options;
const struct TCLIOption * const option = &options;

#define USAGE "                                                      \n\
    %1$s [options] <number of primes>                                \n\
                                                                     \n\
    Calculate <number of primes> in a separate process.              \n\
                                                                     \n\
                                                                     \n\
    OPTIONS                                                          \n\
                                                                     \n\
        -h, --help:      Show this help message.                     \n\
        -v, --verbose:   Show internal information.                  \n\
                                                                     \n\
"

char command_line[MAXSTR];

void
usage (FILE *out) {
    fprintf (out, USAGE, command_line);
}

struct TCLIOption
parse_cli (int argc, char *argv[])
{
    struct TCLIOption opt;
    bzero (&opt, sizeof(opt));

    const char * const short_options = "o:hv";
    const struct option long_options[] = {
        { "output",  1, NULL, 'o' },
        { "help",    0, NULL, 'h' },
        { "verbose", 0, NULL, 'v' },
        { NULL,      0, NULL, 0   }
    };
    int next_option;

    do {
        next_option = getopt_long (
                argc,
                argv,
                short_options,
                long_options,
                NULL
                );
        switch (next_option) {
            case 'o':
                 strncpy(opt.output_filename, optarg, MAXSTR-1);
                 opt.output_filename[MAXSTR-1] = '\0';
                break;
            case 'h':
                opt.help = 1;
                break;
            case 'v':
                opt.verbose = 1;
                break;
            case '?':
                usage (stderr);
                exit (EXIT_FAILURE);
            case -1:
                break;
            default:
                abort ();
        }
    } while (next_option != -1);

    if (optind == argc) {
        usage (stderr);
        exit (EXIT_FAILURE);
    }

    opt.num_primes = atoi (argv[optind]);

    options = opt;

    return opt;
}

void
init_cli (int argc, char *argv[])
{
    strncpy (command_line, argv[0], MAXSTR-1);
    command_line[MAXSTR-2] = '\0';
    parse_cli (argc, argv);
}
