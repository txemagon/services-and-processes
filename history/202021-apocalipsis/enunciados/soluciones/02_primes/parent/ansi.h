#ifndef __ANSI_H__
#define __ANSI_H__

#define SAVE_CURS    "\x1B[s"
#define REST_CURS    "\x1B[u"
#define LOCA_CURS    "\x1B[%i;%if"

#define MVUP_CURS    "\x1B[%iA"
#define DOWN_CURS    "\x1B[%iB"
#define RGHT_CURS    "\x1B[%iC"
#define LEFT_CURS    "\x1B[%iD"

#define RESET        0
#define GREEN        32
#define GREENL      92

#define HIDE_CURS   "\x1B[?25l"
#define SHOW_CURS   "\x1B[?25h"

#define CLEAR_SCR   "\x1B[2J"

#define COLOR        "\x1B[%im"
#define ANS(...)     printf (__VA_ARGS__); fflush(stdout)

#endif
