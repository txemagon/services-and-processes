#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <pthread.h>

#define MAX 20
#define SETCUR      "\x1B[%i;%if"
#define CLEARSCR    "\x1B[2J"
#define CURSAVE     "\x1B[s"
#define CURRES      "\x1B[u"

#define RESCOL      "\x1B[39m"
#define COLGREEN    "\x1B[92m"


int thread_flag;
pthread_cond_t thread_flag_cv;
pthread_mutex_t thread_flag_mutex;

void initialize_flag ()
{
    /* Iniciar el mutex y la variable condicional */
    pthread_mutex_init (&thread_flag_mutex, NULL);
    pthread_cond_init (&thread_flag_cv, NULL);
    /* Inicializar el valor bandera */
    thread_flag = 0;
}

void do_work ()
{
    static int row = 0;
    if (row > 10) {
        printf ( CLEARSCR );
        row = 0;
    }

    printf ( CURSAVE
             COLGREEN );
    printf ( SETCUR, ++row, 40);
    printf ( "Seriously, George?.\n");

    printf ( RESCOL
             CURRES "\n");
}

/* Llamar a do_work repetidamente mientras la bandera esté puesta. Se bloquea con la bandera limpia. */

void *thread_function (void *thread_arg)
{
    static int last_value = thread_flag;

    /* Repetir siempre */
    while (1) {
        /* Bloquear el mutex antes de acceder al valor bandera. */
        pthread_mutex_lock (&thread_flag_mutex);

        while (thread_flag == last_value)
            /* La bandera está abajo. Esperar a que la variable condicional emita una
             * señal  indicando qe ha cambiado el valor de la bandera. Cuando la señal
             * llegue y se desbloquee este hilo, se itera para comprobar la vandera
             * de nuevo */
            pthread_cond_wait (&thread_flag_cv, &thread_flag_mutex);

        /* En este punto tenemos constancia de que la bandera ha sido izada. Por
         * tanto, desbloqueamos el mutex. */
        pthread_mutex_unlock (&thread_flag_mutex);
        do_work ();
        last_value = thread_flag;
    }

    return NULL;
}

void inc_thread_flag ()
{
    /* Bloquear el mutex antes de acceder a la variable bandera. */
    pthread_mutex_lock (&thread_flag_mutex);
    /* Poner la variable bandera y señalarlo si thread_function está bloqueada
     * esperando a que la bandera sea puesta. Notar que la comprobación sólo se
     * hace cuando el mutex se desbloquea. */
    thread_flag++;
    pthread_cond_signal (&thread_flag_cv);
    /* Desbloquear el mutex */
    pthread_mutex_unlock (&thread_flag_mutex);
}

void instructions ()
{
    system ("clear");
    system ("toilet --gay -fpagga MASTERMIND");
    printf ("\n"
            " Tienes que adivinar un número del 0 al %i\n" , MAX
            );
}

int main () {
    int guess;
    int user_guess = -1;
    pthread_t thread_id;

    /* Inicialiación */
    srand (time(NULL));
    initialize_flag ();
    pthread_create (&thread_id, NULL, &thread_function, NULL);
    guess = rand () % MAX;

    instructions ();
    while (user_guess != guess) {
        printf ( SETCUR, 20, 4);
        printf ("\tTu número: ");
        scanf (" %i", &user_guess);
        inc_thread_flag ();
    }

    pthread_join (thread_id, NULL);

    return 0;
}
