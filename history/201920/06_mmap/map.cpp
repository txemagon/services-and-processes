#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <time.h>
#include <unistd.h>

#define FILEN "pantalla.scr"
#define FILE_LENGTH 252

int main() {
    int fichero;
    char *car, *p;

    fichero = open (FILEN, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

    p = car = (char *) mmap (0, FILE_LENGTH, PROT_WRITE, MAP_SHARED, fichero, 0);
    close (fichero);

    for (int i=0; i<FILE_LENGTH; i++, p++)
        if (*p != '#')
            printf ("%c", *p);
        else
            printf ("█");

    printf ("\n");
    munmap (car, FILE_LENGTH);

    return 0;
}
