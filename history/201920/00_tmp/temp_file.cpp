#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

typedef int temp_file_handle;

temp_file_handle write_temp_file (char* buffer, size_t length)
{
    char temp_filename[] = "/tmp/temp_file.XXXXXX";
    int fd = mkstemp (temp_filename);

    unlink (temp_filename);
    write (fd, &length, sizeof (length));
    write (fd, buffer, length);

    return fd;
}

char* read_temp_file (temp_file_handle temp_file, size_t* length)
{
    char* buffer;
    int fd = temp_file;

    lseek (fd, 0, SEEK_SET);
    read (fd, length, sizeof(*length));
    buffer = (char*) malloc (*length);
    read (fd, buffer, *length);
    close (fd);

    return buffer;
}

int main () {


    temp_file_handle anonym_file;
    char mssg[] = "The World is a Vampire";
    size_t mssg_len = strlen(mssg);
    char *yield_mssg;

    anonym_file = write_temp_file (mssg, mssg_len);

    yield_mssg = read_temp_file (anonym_file, &mssg_len);
    printf ("Read: %s\n", yield_mssg );

    free (yield_mssg);

    return EXIT_SUCCESS;
}
