#include "multis.h"

#ifdef __cplusplus
extern "C" {
	#endif
int mul(int op1, int op2);
int div(int op1, int op2);
#ifdef __cplusplus
}
#endif

const char *names[] = {
    "mul",
    "div",
    (char *) 0
};

const char **catalogo () { return names; }
int mul(int op1, int op2) { return op1 * op2; }
int div(int op1, int op2) { return op1 / op2; }
