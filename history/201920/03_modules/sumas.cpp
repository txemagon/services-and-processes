#include "sumas.h"

#ifdef __cplusplus
extern "C" {
	#endif
int mul(int op1, int op2);
int div(int op1, int op2);
#ifdef __cplusplus
}
#endif

const char *names[] = {
    "add",
    "sub",
    (char *) 0
};

const char **catalogo () { return names; }
int add (int op1, int op2) { return op1 + op2; }
int sub (int op1, int op2) { return op1 - op2; }


