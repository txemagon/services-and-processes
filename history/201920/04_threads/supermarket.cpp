#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>

#include "interface.h"
#include "stack.h"

#define LOUNGE   "Harrods.txt"

#define MAX_BUYS   10
FILE *pf = NULL;

void *
buy (void *params)
{
    int n_buys = * (int *) params;

    for (int i=n_buys; i>0; i--) {
        fprintf (pf, "[ID: %lu] Quedan %i compras.\n.", pthread_self (), i);
        fflush (pf);
        usleep (100000);
    }

    free (params);
}

pthread_t
make_new_client ()
{
    pthread_t id;

    /* n_buys is a dinamically create param
     * for the new thread. Each thread would be
     * responsible of freeing memory allocation
     **/
    int *n_buys = (int *) malloc (sizeof (int));
    *n_buys = rand () % MAX_BUYS + 1;

    pthread_create (&id, NULL, &buy, n_buys);

    return id;
}

void
begin (struct TStack *st_client)
{

    pf = fopen (LOUNGE, "w");
    if (!pf) {
        fprintf (stderr, "No hemos encontrado Harrods.\n");
        exit (1);
    }
    init (st_client);
    srand (time (NULL));
}

void
end ()
{
   fclose (pf);
}

int
main (int argc, char *argv[])
{
    bool closed = false;
    enum MenuOption menu_option;    /* Holds menu options within a range */
    struct TStack st_client;        /* Thread pool */

    begin (&st_client);

    /* We need an Interface to allow clients in. */
    while (!closed)
    {
        menu_option = menu ();      /* The menu offers you, our particular god */
                                    /* to create muddy clients or close the SM */

        switch (menu_option)
        {
            case new_client:        /* Then, we create a new thread and we add it*/
              push (&st_client, make_new_client ());   /* to the pool of threads */
                break;
            case close_sm:
                closed = true;
                for (int i=0; i<st_client.s; i++)
                    pthread_join (st_client.data[i], NULL);
                break;
        }
    }

    end ();

    return EXIT_SUCCESS;
}
