#include "interface.h"

/* In this way we can associate the menu option
 * with an action (function pointer).
 *
struct TOption
{
    const char *name;
    void *(*action)(void *params);
} menu_options[] = {
    {"Crear cliente", NULL},
    {"Cerrar el super", NULL}
};
 * To call option number "option" we just:
 *      (*menu_options[option].action)(parameters)
 */

/* In order to simplify we just fall back to a switch case */
const char *menu_options[] =
{
    "Crear cliente",
    "Cerrar el super"
};

enum MenuOption
menu ()
{
    int option;

    do {
    system ("clear");
    system ("toilet --gay -f pagga SUPERMERCADO");
    printf (
            "\n"
            "\n"
           );

    for (int i=0; i<TOTAL_MENU_OPT; i++)
        printf ("       %i.- %s.\n", i + 1, menu_options[i]);

    printf (
            "\n"
            "       Opcion: "
            );
    scanf (" %i", &option);
    } while (option<1 || option>TOTAL_MENU_OPT);

    return (enum MenuOption) --option;
}

