#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>

#define NPLAYS  4
#define NINOS  14

sig_atomic_t a_cenar = 0;
sig_atomic_t plays_usadas = 0;
sem_t semaforo;

void
inicializar (int n_plays)
{
  sem_init (&semaforo, 0, n_plays);
}

void *
say (void *param)
{
  while (!a_cenar)
    {
      sem_wait (&semaforo);
      plays_usadas++;
      sleep (rand () % 3 + 1);
      plays_usadas--;
      sem_post (&semaforo);
      sleep (rand () % 30 + 1);
    }
}

int
main (int argc, char *argv[])
{
    struct timeval tv;
    time_t curtime;
    char tiempo[32];
    uint8_t count = 0xFF;

  pthread_t nino[NINOS];

  srand (time (NULL));
  inicializar (NPLAYS);
  for (int i = 0; i < NINOS; i++)
    pthread_create (&nino[i], NULL, &say, (void *) NULL);

  while(count) {
      count--;
      gettimeofday(&tv, NULL);
      curtime = tv.tv_sec;
      strftime(tiempo, 32, "%T", localtime(&curtime));
      printf(" [%s] Usadas: %i                   \r",
              tiempo, plays_usadas);
      fflush(stdout);
      usleep(500000);
  }

  a_cenar = 1;

  for (int i=0; i<NINOS; i++)
      pthread_join(nino[i], NULL);

  sem_destroy(&semaforo);

  return EXIT_SUCCESS;
}
