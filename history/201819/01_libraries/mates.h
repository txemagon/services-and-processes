#ifndef __MATES_H__
#define __MATES_H__

#ifdef __cplusplus
extern "C" {
#endif

double suma (double op1, double op2);
double rest (double op1, double op2);
double mult (double op1, double op2);
double divi (double op1, double op2);

#ifdef __cplusplus
}
#endif

#endif
