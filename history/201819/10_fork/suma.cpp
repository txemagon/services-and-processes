#include <stdlib.h>

int
main (int argc, char *argv[])
{

    if (argc< 3)
        abort ();

    return atoi(argv[1]) + atoi(argv[2]);
}
