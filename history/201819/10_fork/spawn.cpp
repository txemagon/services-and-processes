#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

pid_t
spawn (const char *comando, char *lc [])
{

    pid_t child_pid = fork ();

    if (child_pid != 0)
    {
        /* Soy el padre */
    }
    else
    {
        /* Soy el hijo */
        execvp (comando, lc);
        fprintf (stderr, "No he podido arrancar el firefox.\n");
        abort ();
    }
    return child_pid;
}

int
main (int argc, char *argv [])
{
    pid_t child_pid;
    int child_status;
    int resultado;

    /* Otras cosas */

    child_pid = spawn ("./suma", argv);
    wait (&child_status);

    if (WIFEXITED(child_status)) {
        resultado = WEXITSTATUS(child_status);
        printf ("La suma vale %i\n", resultado);
    }


    return EXIT_SUCCESS;
}
