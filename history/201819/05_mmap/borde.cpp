#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define FICHERO "pepe.txt"
#define TAM 0x100

int
main (int argc, char *argv[])
{
    const char borde[] = {
        "***********\n"
        "*         *\n"
        "*         *\n"
        "*         *\n"
        "*         *\n"
        "*         *\n"
        "*         *\n"
        "***********\n"
    };

    int fd;
    char *mapa = NULL;

   fd =  open(FICHERO, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

   lseek (fd, TAM+1, SEEK_SET);
   write (fd, "", 1);
   lseek (fd, 0, SEEK_SET);
   mapa = (char *) mmap(0, TAM, PROT_WRITE, MAP_SHARED, fd, 0);

   close(fd);
   memcpy(mapa, borde, sizeof(borde));

   munmap(mapa, TAM);
  return EXIT_SUCCESS;
}
