#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#define R    0
#define W    1
#define DOWN 0
#define UP   1

int
main (int argc, char *argv[])
{
    int fd[2][2];
    pid_t my_son;
    int resultado;

    pipe(fd[DOWN]);
    pipe(fd[UP]);

    my_son = fork();
    if (my_son){
        close(fd[DOWN][R]);
        close(fd[UP][W]);

        write(fd[DOWN][W], "3+4\n", 4);
        FILE *stream =fdopen(fd[UP][R], "r");
        fscanf(stream, " %i", &resultado);
        write(fd[DOWN][W], "quit\n", 5);

        printf("Dice BC que 3 + 4 = %X\n", resultado);
        close(fd[DOWN][W]);
        close(fd[UP][R]);
        waitpid(my_son, NULL, 0);
    } else {
        close(fd[DOWN][W]);
        close(fd[UP][R]);

        dup2(fd[DOWN][R], STDIN_FILENO);
        dup2(fd[UP][W], STDOUT_FILENO);

        execlp("bc", "bc", NULL);
        fprintf(stderr, "No he podido arrancar a bc.\n");

        close(fd[DOWN][R]);
        close(fd[UP][W]);

        abort();
    }


    return EXIT_SUCCESS;
}
