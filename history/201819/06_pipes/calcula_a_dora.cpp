#include <stdio.h>
#include <strings.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#define R    0
#define W    1
#define DOWN 0
#define UP   1

#define N 0x100

int
main (int argc, char *argv[])
{
    int fd[2][2];
    pid_t my_son;
    int resultado;

    pipe(fd[DOWN]);
    pipe(fd[UP]);

    my_son = fork();
    if (my_son){
        close(fd[DOWN][R]);
        close(fd[UP][W]);
        FILE *stream =fdopen(fd[UP][R], "r");
        char buffer[N];

        do {
            printf("BC =>: ");
            fgets( buffer, N, stdin );
            write( fd[DOWN][W], buffer, strlen(buffer) );
            fscanf(stream, " %i", &resultado);
            printf("<= BC: %i\n", resultado);
        } while ( strcmp(buffer, "quit") );


        close(fd[DOWN][W]);
        close(fd[UP][R]);
        waitpid(my_son, NULL, 0);
    } else {
        close(fd[DOWN][W]);
        close(fd[UP][R]);

        dup2(fd[DOWN][R], STDIN_FILENO);
        dup2(fd[UP][W], STDOUT_FILENO);

        execlp("bc", "bc", NULL);
        fprintf(stderr, "No he podido arrancar a bc.\n");

        close(fd[DOWN][R]);
        close(fd[UP][W]);

        abort();
    }


    return EXIT_SUCCESS;
}
