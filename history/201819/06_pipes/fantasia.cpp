#include <stdio.h>
#include <strings.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>

#define R 0
#define W 1

int
main (int argc, char *argv[])
{
    int fds[2];
    FILE *tubo;
    struct sigaction sa;
    pid_t son;

    pipe(fds);

    if (son){
        /* Dark Vader*/
        close(fds[R]);
        tubo = fdopen(fds[W], "w");
        fprintf(tubo, "The world is a vampire.%i", EOF);
        close(fds[W]);
        wait(&son);
    } else {
        /* Luke Skywalker */
        close(fds[W]);
        tubo = fdopen(fds[R], "r");

        execl("./errante", "./errante", NULL);
        fprintf(stderr, "No he podido arrancar a errante.\n");
        abort();
        close(fds[R]);
    }


    return EXIT_SUCCESS;
}
