#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void
f (int n)
{
    printf ("n: %i\n", n);
    if (n)
        f (n-1);
}

int
main (int argc, char *argv[])
{
  f(7);

  return EXIT_SUCCESS;
}
