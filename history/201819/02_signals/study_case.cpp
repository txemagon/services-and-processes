#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t continuo = 0;

void
gestor_sigterm (int signal)
{
  continuo = 1;
}

void
gestor_sigint (int signal)
{
  continuo = 2;
}

int
main (int argc, char *argv[])
{
  struct sigaction sa;
  bzero (&sa, sizeof (sa));
  sa.sa_handler = &gestor_sigterm;
  sigaction (SIGTERM, &sa, NULL);
  sa.sa_handler = &gestor_sigint;
  sigaction (SIGINT, &sa, NULL);


  printf ("Hey, bro. My PID: %i\n", getpid ());

  while(continuo == 0)
      sleep (1);

  printf ("Función de salida: %s\n", continuo == 1? "sigterm" : "sigint");

  return EXIT_SUCCESS;
}
