#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t continuo = 1;

void
cuentame (int signal)
{
  continuo = 0;
}

int
main (int argc, char *argv[])
{
  struct sigaction sa;
  bzero (&sa, sizeof (sa));
  sa.sa_handler = &cuentame;
  sigaction (SIGINT, &sa, NULL);

  printf ("Hey, bro. My PID: %i\n", getpid ());

  while(continuo)
      sleep (1);

  printf ("Ahora barro mi casita\n");

  return EXIT_SUCCESS;
}
