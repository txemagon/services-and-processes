#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>

sig_atomic_t count = 0;

void
cuentame (int signal)
{
  count++;
}

int
main (int argc, char *argv[])
{
  struct sigaction sa;
  bzero (&sa, sizeof (sa));
  sa.sa_handler = &cuentame;
  sigaction (SIGUSR1, &sa, NULL);

  printf ("Hey, bro. My PID: %i\n", getpid ());

  sleep (20);

  printf ("Signal Count: %i\n", count);

  return EXIT_SUCCESS;
}
