#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


//#define NDEBUG

#ifndef NDEBUG
#define DEBUG(x) fprintf(stderr, "DBG (Line: %i): %s\n",__LINE__, (x));
#else
#define DEBUG(x)
#endif

int
main (int argc, char *argv[])
{
  DEBUG ("Iniciando Programa.");

  return EXIT_SUCCESS;
}
